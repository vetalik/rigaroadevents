package hello;

import org.joda.time.DateTime;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;


import com.sun.syndication.feed.synd.*;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class FeedReader {

    private Map<String, String> getDataByLink(String url) throws IOException {
        Map<String, String> res = new HashMap();
        Document doc = Jsoup.connect(url).get();
        Elements imgEl = doc.select("img.float_right");
        Elements content = doc.select("#_ctl1_contentControl");
        String img = "";
        String text = "";
        String signature = "";
        try{
            img = imgEl.get(0).attributes().get("src");
        } catch (Exception e) {}

        try{
            text = content.get(0).childNodes().get(0).childNode(0).toString();
        } catch (Exception e) {}

        try{
            signature = content.get(0).childNodes().get(2).childNode(1).toString();
        } catch (Exception e) {}

        res.put("img", img);
        res.put("text", text);
        res.put("signature", signature);
        return res;
    }

    public List<RigaEventData> getRoadRestrictions(String url) {
        boolean ok = false;

        List restrictions = new ArrayList();
        List<RigaEventData> events = new ArrayList();

        try {
            URL feedUrl = new URL(url);

            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(new XmlReader(feedUrl));

            for (Object el : feed.getEntries()) {

                List el2 = ((SyndEntryImpl) el).getCategories();
                SyndCategory cat =((SyndCategoryImpl) el2.get(0));
                //DCSubjectImpl subj = ((SyndCategoryImpl) cat).getSubject();
                String category = cat.getName();

                if (category.equals("Satiksmes ierobežojumi")) {
                    restrictions.add(el);
                }
                //see data on each entry here http://dz.prosyst.com/pdoc/mBS_SH_SDK_7.4.1/modules/rss/api/com/sun/syndication/feed/synd/SyndEntryImpl.html
            }
            
            for (Object el1: restrictions){
                SyndEntryImpl element = (SyndEntryImpl) el1;
                DateTime dt = new DateTime(element.getPublishedDate());
                List el3 = element.getCategories();
                SyndCategory cat1 =((SyndCategoryImpl) el3.get(0));
                String fullDescr = "";
                String img = "";
                String signature = "";

                Map<String, String> res = new HashMap();
                try{
                    res = getDataByLink(element.getLink());

                }catch (Exception e){}

                fullDescr = res.get("text");
                img = res.get("img");
                signature = res.get("signature");

                String author = "";
                if (signature != null && signature.length() > 0){
                    author = signature;
                } else {
                    author = element.getAuthor();
                }

                //res = getDataByLink(element.getLink());
                RigaEventData data = new RigaEventData(
                    element.getLink(),
                    element.getTitle(),
                    element.getLink(),
                    element.getDescription().getValue(),
                    author,
                    cat1.getName(), // Category
                    dt,
                    fullDescr,
                    img
                );
                events.add(data);
            }
            ok = true;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ERROR: "+ex.getMessage());
        }

        return events;
    }

}