'use strict';

describe('Service: reader', function () {

  // load the service's module
  beforeEach(module('srcApp'));

  // instantiate service
  var reader;
  beforeEach(inject(function (_reader_) {
    reader = _reader_;
  }));

  it('should do something', function () {
    expect(!!reader).toBe(true);
  });

});
