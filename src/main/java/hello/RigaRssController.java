package hello;

import hello.RigaEventData;
import hello.FeedReader;

import org.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Sort;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashMap;
import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import org.json.JSONObject;

import com.google.common.collect.Lists;

import java.util.List;


@Controller
public class RigaRssController {
    @Autowired
    EventDataMongoRepository repository;

    @RequestMapping("/riga")
    public @ResponseBody List<RigaEventData> rigaEvents( Model model) {
        List<String> sortOrder = Arrays.asList("id");
        List<RigaEventData> events = Lists.newArrayList(repository.findAll(new Sort(Sort.Direction.DESC, sortOrder)));
        return events;
    }
    
    @RequestMapping(value="/event", method = RequestMethod.GET)
    public @ResponseBody RigaEventData rigaEvent(@RequestParam(value="eventId") String eventId, Model model) throws UnsupportedEncodingException {
        RigaEventData event = repository.findOne(eventId);
        return event;
    }
    
    @RequestMapping(value="/event/put", method = RequestMethod.POST , consumes = "application/json")
    public @ResponseBody RigaEventData rigaEventUpdate(@RequestBody LinkedHashMap payload,  Model model){
        JSONObject pay = new JSONObject(payload);
        JSONObject paths = pay.getJSONObject("paths");
        String eventId = pay.getString("eventId");

        RigaEventData event = repository.findOne(eventId);
        event.setPathsJson(paths.toString());
        repository.save(event);
        return event;
    }
    
    @RequestMapping("/refresh")
    public @ResponseBody String refreshevents(Model model) {
        FeedReader a =new FeedReader();
        List<RigaEventData> events = a.getRoadRestrictions("https://www.riga.lv/rss/lv/news/");
        try {
            for (RigaEventData event: events){
                List<RigaEventData> l = repository.findByGuid(event.getGuid());
                if (l.size() == 0) {
                    repository.save(event);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return "Done";
    }

}