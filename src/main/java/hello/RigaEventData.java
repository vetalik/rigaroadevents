package hello;

import org.joda.time.DateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;
//import org.springframework.data.annotation.Id;


@Entity
public class RigaEventData {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private String id;
    private String guid;
    private String title;
    private String link;
    private String description;
    private String author;
    private String category;
    private String fullDescr;
    private String img;
    private String pubDate;
    private String pathsJson;

    protected RigaEventData(){
        this.guid = "";
        this.title = "";
        this.link = "";
        this.description = "";
        this.author = "";
        this.category = "";
        this.pubDate = "";
        img = "";
        fullDescr = "";
        this.pathsJson = "";
    }

    public RigaEventData(String guid, String title, String link, String description, String author, String category, DateTime pubDate, String fullDescr, String img)throws UnsupportedEncodingException {
        this.guid = URLEncoder.encode(guid, "UTF-8");
        this.title = title;
        this.link = link;
        this.description = description;
        this.author = author;
        this.category = category;
        this.fullDescr = fullDescr;
        this.img = img;
        this.pubDate = pubDate.toString();
        this.pathsJson = "";
    }

    public String getGuid() {
        return guid;
    }
    
    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
    
    public String getLink() {
        return link;
    }
    public String getDescription() {
        return description;
    }
    public String getAuthor() {
        return author;
    }
    public String getCategory() {
        return category;
    }
    public String getFullDescr() {return fullDescr;}
    public  String getImg() {return  img;}
    public String getPubDate() {
        return pubDate;
    }
    
    public String getPathsJson(){return this.pathsJson;}
    public void setPathsJson(String pathsJson){this.pathsJson=pathsJson;}

}