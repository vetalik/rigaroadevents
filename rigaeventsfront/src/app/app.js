
var myApp = angular.module('rigaeventsfrontApp', [
    'ngRoute',
    'ui.router',
    'ngResource',
    'ngMaterial',
    'leaflet-directive',
]);

myApp.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/");
  //
  // Now set up the states
  $stateProvider
    .state('state1', {
      url: "/",
      templateUrl: "templates/events.html",
      controller: "EventsListCtrl"
    })
    .state('map', {
      url: "/map",
      templateUrl: "templates/map.html",
      controller: "MapCtrl"
    })
    .state('event', {
      url: "/event/:eventId",
      templateUrl: "templates/event.html",
      controller: "EventCtrl"
    });
});

myApp.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    // .primaryPalette('teal')
    // .accentPalette('teal')
    // .backgroundPalette('wheat');
    
    .primaryPalette('pink', {
      'default': '400', // by default use shade 400 from the pink palette for primary intentions
      'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
      'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
      'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
    })
    .accentPalette('teal')
});
