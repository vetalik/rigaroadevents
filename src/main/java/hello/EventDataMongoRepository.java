package hello;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "events", path = "events")
public interface EventDataMongoRepository extends MongoRepository<RigaEventData, String> {

    List<RigaEventData> findByGuid(@Param("guid") String guid);

}