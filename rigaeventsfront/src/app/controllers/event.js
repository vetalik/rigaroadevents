'use strict';

/**
 * @ngdoc function
 * @name rigaeventsfrontApp.controller:EventCtrl
 * @description
 * # EventCtrl
 * Controller of the rigaeventsfrontApp
 */
angular.module('rigaeventsfrontApp')
  .controller('EventsListCtrl', function ($scope, reader) {

    reader.query().$promise.then(function(data){
        $scope.AAA = data;
        angular.forEach($scope.AAA, function(value, key){
            if (value.pathsJson && value.pathsJson.length>0){
                
                var path = JSON.parse(value.pathsJson);
                value.path = {};
                value.path = path;
                value.center = path[1].latlngs[0];
                value.center.zoom = 16;
                console.log(value.center);
            } else {
                value.path = null;
            }
        });
    });

  })
  .controller('EventCtrl', function ($scope, $stateParams, leafletData, event, eventsave) {
    var id=$stateParams.eventId;

        $scope.paths = {};
        $scope.path_num = 0;
        $scope.center = {};

    event.get({eventId: id}).$promise.then(function(data){
      $scope.event = data;
      $scope.pathsJson = JSON.parse(data.pathsJson);

      $scope.paths = $scope.pathsJson;
      
      $scope.center.lat = $scope.pathsJson[1].latlngs[0].lat;
      $scope.center.lng = $scope.pathsJson[1].latlngs[0].lng
      $scope.center.zoom = 16;
      console.log("aaaa", $scope.center);
    });

    $scope.savepaths = function(){
        if ($scope.newPath){
            eventsave.save({'paths': $scope.paths, 'eventId': id});
        }
    };

    leafletData.getMap().then(function(map) {
//             new L.Control.GeoSearch({
//                 provider: new L.GeoSearch.Provider.OpenStreetMap(),
//                 draggable: true
//             }).addTo(map);

        // create a map in the "map" div, set the view to a given place and zoom

        // add an OpenStreetMap tile layer
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        // Initialise the FeatureGroup to store editable layers
        var drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);

        // Initialise the draw control and pass it the FeatureGroup of editable layers
        var drawControl = new L.Control.Draw({
            edit: {
                featureGroup: drawnItems
            }
        });
        map.addControl(drawControl);

        map.on('draw:created', function (e) {
            var type = e.layerType,
                layer = e.layer;
            console.log(layer);
            $scope.newPath = layer._latlngs;
            $scope.path_num = $scope.path_num +1;
            $scope.paths[$scope.path_num] = {
                color: 'red',
                weight: 4,
                latlngs: $scope.newPath
            }
            // Do whatever else you need to. (save to db, add to map etc)
            map.addLayer(layer);
        });

    });
  });
