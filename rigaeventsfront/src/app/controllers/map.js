'use strict';

angular.module('rigaeventsfrontApp')
  .controller('MapCtrl', function ($scope, leafletData) {
    $scope.paths = {};
    angular.extend($scope, {
        taipei: {
            lat: 25.0391667,
            lng: 121.525,
            zoom: 6
        },
        paths: $scope.paths,
        defaults: {
            scrollWheelZoom: false
        }
    });

    leafletData.getMap().then(function(map) {
//             new L.Control.GeoSearch({
//                 provider: new L.GeoSearch.Provider.OpenStreetMap(),
//                 draggable: true
//             }).addTo(map);

        // create a map in the "map" div, set the view to a given place and zoom

        // add an OpenStreetMap tile layer
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        // Initialise the FeatureGroup to store editable layers
        var drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);

        // Initialise the draw control and pass it the FeatureGroup of editable layers
        var drawControl = new L.Control.Draw({
            edit: {
                featureGroup: drawnItems
            }
        });
        map.addControl(drawControl);

        map.on('draw:created', function (e) {
            var type = e.layerType,
                layer = e.layer;
            console.log(layer);
            $scope.newPath = layer._latlngs;
            $scope.paths.p1 = {
                color: '#008000',
                weight: 8,
                latlngs: $scope.newPath
            }
            // Do whatever else you need to. (save to db, add to map etc)
            map.addLayer(layer);
        });

    });
  });
