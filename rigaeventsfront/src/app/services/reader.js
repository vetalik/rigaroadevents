'use strict';

/**
 * @ngdoc service
 * @name srcApp.reader
 * @description
 * # reader
 * Service in the srcApp.
 */
//var base_url = "http://localhost:8080/";
var base_url = "http://uvkkda99a593.vetalik.koding.io:8080/";
angular.module('rigaeventsfrontApp')
  .service('reader', function ($resource) {
    return $resource(base_url + 'riga');
  })
  .service('event', function ($resource) {
    return $resource(base_url + 'event');
  })
  .service('eventsave', function ($resource) {

    return $resource(base_url + 'event/put');
  });
